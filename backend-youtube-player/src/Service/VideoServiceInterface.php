<?php

namespace App\Service;

use App\Entity\Video;
use Doctrine\ORM\EntityManagerInterface;


/**
 *
 */
interface VideoServiceInterface
{
    /**
     * Get the objects necesary for DB integration
     */
    public function __construct(EntityManagerInterface $entityManager);

    /**
     * Return all the videos entities of the DB
     * 
     * @return Video[]
     */
    public function getAllVideos();

    /**
     * Return the youtubeId from a video entities array
     * 
     * @param array $videos Array containing all the videos
     * 
     * @return String[]
     */
    public function getYoutubeIdsFromVideos(array $videos);

    /**
     * Store one youtube Id to the Database
     * 
     * @param string $youtubeId The id of the video to be stored
     * 
     * @return void
     */
    public function storeYoutubeId(string $youtubeId);
}
