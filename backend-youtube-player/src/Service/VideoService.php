<?php

namespace App\Service;

use App\Entity\Video;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Service to interact with video database
 */
class VideoService implements VideoServiceInterface
{

    private $_entityManager;

    private $_repository;

    /**
     * Get the objects necesary for DB integration
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->_entityManager = $em;
        $this->_repository = $em->getRepository(Video::class);
    }

    /**
     * Return all the videos entities of the DB
     * 
     * @return Video[] $video array containing all the videos
     */
    public function getAllVideos()
    {
        $videos = $this->_repository->findall();
        return $videos;
    }

/**
     * Return the youtubeId from a video entities array
     * 
     * @param array $videos Array containing all the videos
     * 
     * @return String[]
     */
    public function getYoutubeIdsFromVideos(array $videos)
    {
        $youtubeIds = array();

        $getYoutubeID = function ($video) use ($youtubeIds) {
            $youtubeId = $video->getYoutubeId();
            return $youtubeId;
        };
        $youtubeIds = array_map($getYoutubeID, $videos);
        return $youtubeIds;
    }

    /**
     * Store one youtubeId to the Database
     * 
     * @param string $youtubeId the id of the video to be stored
     * 
     * @return void
     */
    public function storeYoutubeId(string $youtubeId)
    {
        $video = new Video();
        $video->setYoutubeId($youtubeId);
        $this->_entityManager->persist($video);
        $this->_entityManager->flush();
    }
}
