<?php

namespace App\Controller;

use App\Entity\Video;
use App\Service\VideoServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Backend for the custom youtube player app
 */
class YoutubeController extends Controller
{

    /**
     * Store a youtubeId to database
     * 
     * @Route("/store-history", methods={"POST"})
     *
     * @param Request $request HTTP request object

     * @return Response
     **/
    public function storeVideoToHistoryAction(
        Request $request,
        VideoServiceInterface $videoService
    ) {

        $youtubeId = $request->request->get('id');

        $videoService->storeYoutubeId($youtubeId);

        return new Response("ok", Response::HTTP_OK);
    }

    /**
     * Returns the youtubeId of all the videos stored in the remote history
     *
     * @Route("/retrieve-history", methods={"GET"})
     *
     * @var VideoService $videoService implements the service VideoService
     *
     * @return Response
     **/
    public function retrieveVideoHistory(
        VideoServiceInterface $videoService
    ) {

        $videos = $videoService->getAllVideos();
        $youtubeIds = $videoService->getYoutubeIdsFromVideos($videos);

        $response = new JsonResponse($youtubeIds);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
