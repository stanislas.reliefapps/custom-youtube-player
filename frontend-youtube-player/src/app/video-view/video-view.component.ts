import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { Video } from '../video';
import { UrlService } from '../url.service';


const embedPreUrl = "http://www.youtube.com/embed/"

@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.css']
})


export class VideoViewComponent implements OnInit {
  embedUrl: SafeUrl;

  constructor(
    private urlService: UrlService,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
    this.urlService.urlEventEmitter.subscribe(
      (video:Video) => this.embedUrl = this.sanitize(embedPreUrl + video.id)
    );
  }

  sanitize(url:string):SafeUrl{
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
