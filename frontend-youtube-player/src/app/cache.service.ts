import { Injectable } from '@angular/core';

import { Local } from './local';

const localDataKey = "local";
@Injectable({
  providedIn: 'root'
})
export class CacheService extends Local{

  constructor() {
    super(localDataKey);
  }
}
