import { Injectable } from '@angular/core';

import { Local } from './local';

const localDataKey = "failsafe";

@Injectable({
  providedIn: 'root'
})
export class FailsafeService extends Local {

  constructor() {
    super(localDataKey);
  }

  public hasItems(): boolean {
    const items = this.getAll();
    if (items) {
      return true;
    }
    return false;
  }
}
