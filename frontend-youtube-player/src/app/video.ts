const rick = 'dQw4w9WgXcQ';
const baseUrl= 'https://www.youtube.com/watch?v='

export class Video{
  url:string;
  id:string;


  constructor(urlOrId:string){
    if(urlOrId.length==11){
      this.generateFromId(urlOrId)
    }
    else if(urlOrId.includes(baseUrl)){
      this.generateFromUrl(urlOrId)
    }
    else{
      this.id = rick;
      this.url = baseUrl+this.id;
    }

  }

  generateFromUrl(url:string){
    this.url = url;
    this.id = this.generateId(url);
  }

  generateFromId(id:string){
    this.id = id;
    this.url = this.generateUrl(id);
  }

  generateId(url:string):string{
    return url.split(baseUrl)[1];
  }

  generateUrl(id:string):string{
    return baseUrl + id;
  }
}
