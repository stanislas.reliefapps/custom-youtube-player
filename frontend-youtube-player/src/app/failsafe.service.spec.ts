import { TestBed } from '@angular/core/testing';

import { FailsafeService } from './failsafe.service';

describe('FailsafeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FailsafeService = TestBed.get(FailsafeService);
    expect(service).toBeTruthy();
  });
});
