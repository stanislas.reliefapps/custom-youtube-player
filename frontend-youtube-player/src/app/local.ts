export class Local {

  protected localDataKey: string;

  constructor(localDataKey: string) {
    this.localDataKey = localDataKey;
  }

  public push(value: string) {

    let values = this.getAll();

    if (values) {
      values.push(value);
    } else {
      values = [value];
    }

    this.setAll(values);
  }

  public setAll(values: string[]) {
    localStorage.setItem(this.localDataKey, JSON.stringify(values));
  }

  public getAll(): string[] {
    return JSON.parse(localStorage.getItem(this.localDataKey));
  }

  public flush() {
    this.setAll([]);
  }
}
