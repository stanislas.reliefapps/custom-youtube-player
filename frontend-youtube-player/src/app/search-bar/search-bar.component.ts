import { Component, OnInit } from '@angular/core';

import { UrlService } from '../url.service'
import { Video } from '../video'

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  urlInput:string;

  constructor(private urlService: UrlService) { }

  ngOnInit() {
    this.urlService.urlEventEmitter.subscribe(
      (video:Video) => this.urlInput = video.url
    )
  }

  onSubmit(){
    const url = this.urlInput.trim();
    if(url){
      this.urlService.setUrl(url)
    }
  }
}
