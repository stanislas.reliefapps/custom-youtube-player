import { Component, OnInit } from '@angular/core';

import { UrlService } from '../url.service';
import { RemoteService } from '../remote.service';
import { CacheService } from '../cache.service';
import { Video } from '../video';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  private videos: Video[] = [];

  constructor(
    private urlService: UrlService,
    private remoteService: RemoteService,
    private cacheService: CacheService,
  ) { }

  public ngOnInit() {
    this.videos = this.getLocalHistory();

    this.remoteService.getAll()
      .subscribe((ids: string[]) => {
        this.videos = this.idsToVideos(ids);
        this.cacheService.setAll(ids);
      });

    this.urlService.urlEventEmitter
      .subscribe((video: Video) => this.addVideo(video));
}

  public addVideo(video: Video): void {

    this.videos.push(video);

    this.cacheService.push(video.id);
    this.remoteService.push(video.id)
      .subscribe();
  }

  private getLocalHistory(): Video[] {
    const ids = this.cacheService.getAll();
    return (ids ? this.idsToVideos(ids) : []);
  }

  private idsToVideos(ids: string[]): Video[]{
    const videos = [];

    for (const id of ids) {
      const video = new Video(id);
      videos.push(video);
    }

    return videos;
  }
}
