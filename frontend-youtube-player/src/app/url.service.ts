import { Injectable, EventEmitter } from '@angular/core';

import { Video } from './video';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  currentVideo: Video;
  urlEventEmitter = new EventEmitter();

  constructor() { }

  setUrl(url: string): void {
    this.currentVideo = new Video(url);
    this.urlEventEmitter.emit(this.currentVideo);
  }
}
