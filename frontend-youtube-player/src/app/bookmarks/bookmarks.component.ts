import { Component, OnInit } from '@angular/core';

import { Video } from '../video';
import { UrlService } from '../url.service';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {

  bookmarks: Video[]=[];

  constructor(private urlService:UrlService) { }

  ngOnInit() {
  }

  bookmark():void{
    const currentVideo = this.urlService.currentVideo;
    if (currentVideo){
      if(!this.bookmarks.includes(currentVideo)){
        this.bookmarks.push(currentVideo);
      }

    }
  }

  delete(video:Video){
    this.bookmarks = this.bookmarks.filter(bookmark => bookmark !== video)
  }


}
