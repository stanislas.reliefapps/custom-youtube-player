import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';

import { FailsafeService } from "./failsafe.service";
import { throwError } from 'rxjs';

const baseUrl = 'http://127.0.0.1:8000/'
const getUrl = baseUrl + 'retrieve-history';
const postUrl = baseUrl + 'store-history';

@Injectable({
  providedIn: 'root'
})
export class RemoteService {

  constructor(
    private http: HttpClient,
    private failsafe: FailsafeService,
  ) { }

  public getAll() {
    return this.http.get(getUrl)
      .pipe(
        tap(() => this.pushFromFailsafe())
      );
  }

  public push(youtubeId: string) {
    return this.http.post(
      postUrl,
      this.setBody(youtubeId),
      { responseType: "text" },
    )
    .pipe(
      catchError((err: any) => {
        this.failsafe.push(youtubeId);
        return throwError(err);
      }));
  }

  private pushFromFailsafe() {
    if (this.failsafe.hasItems()) {
      const items = this.failsafe.getAll();
      for ( const item of items ) {
        this.push(item).subscribe(() => {
          location.reload();
        });
      }
      this.failsafe.flush();
    }
  }

  private setBody(youtubeId: string) {
    return { id: youtubeId };
  }

}
